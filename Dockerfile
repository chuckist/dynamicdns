FROM redhat/ubi8-minimal
ENV PROVIDER=https://dynamicdns.pairdomains.com/ajax
ENV INTERVAL=600
WORKDIR /
COPY register.sh /register.sh
RUN chmod 550 /register.sh
ENTRYPOINT ["/register.sh"]