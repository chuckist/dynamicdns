#!/bin/bash
echo Start Registration
while :
do
  echo ============================================================
  echo Register provider:$PROVIDER key:$KEY 
  for HOSTNAME in $HOSTNAMES
    do
      echo ------------------------------------------------------------
      echo Register hostname:$HOSTNAME
      curl -d "key=$KEY&hostname=$HOSTNAME&enable=1&tos_agree=on&dns_tos_agree=on" -X POST $PROVIDER
  done
  sleep $INTERVAL
done