# easydyndns

## Build Steps
```
docker build -t dynamicdns:latest .
```

## Run Steps
```
docker container run --env PROVIDER=https://dynamicdns.pairdomains.com/ajax --env KEY=secret-key --env HOSTNAMES="my1.hostname.com my2.hostname.com" --env INTERVAL=600 --name easydyndns --rm  dynamicdns:latest
```